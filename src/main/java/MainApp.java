import com.google.inject.Guice;
import com.google.inject.Injector;
import config.Config;
import config.GuiceFXMLLoader;
import config.GuiceModule;
import controllers.utils.AlertMessage;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import view.FxmlView;
import view.NodeFxmlViewPool;
import view.StageFxmlViewStagePool;

import java.io.File;
import java.util.Locale;

public class MainApp extends Application {

    private static Logger logger = LogManager.getLogger(MainApp.class);

    public static void main(String[] args) {

        if (Config.IS_ANY_INSTANCE) {
            System.exit(0);
        } else {
            launch(args);
        }
    }

    @Override
    public void init() {

        Locale.setDefault(Locale.CANADA);

        initLogger();
    }

    @Override
    public void start(Stage stage) {

        Injector injector = Guice.createInjector(new GuiceModule());

        initGuice(injector);

        show(injector);

        Thread.currentThread().setUncaughtExceptionHandler((thread, throwable) -> {
            logger.error(throwable.getClass(), throwable);
            AlertMessage.showErrorMessage(throwable.getClass().getClass() + "", throwable.getMessage());
        });
    }

    private void initGuice(Injector injector) {

        injector.injectMembers(new GuiceFXMLLoader(injector));

        injector.injectMembers(new StageFxmlViewStagePool(injector.getInstance(GuiceFXMLLoader.class)));

        injector.injectMembers(new NodeFxmlViewPool(injector.getInstance(GuiceFXMLLoader.class)));
    }

    private void show(Injector injector) {

        injector.getInstance(StageFxmlViewStagePool.class).show(FxmlView.MAIN);
    }

    private void initLogger() {

        LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);

        File file = new File("resources/log4j2.xml");

        context.setConfigLocation(file.toURI());
    }
}
