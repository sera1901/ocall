package config;

import com.google.inject.AbstractModule;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GuiceModule extends AbstractModule {
    @Override
    protected void configure() {
        //bind(AccessDao.class).to(AccessModel.class);

        bind(ExecutorService.class).toInstance(Executors.newFixedThreadPool(7));
    }
}
