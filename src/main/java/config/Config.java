package config;


import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;


public class Config {

    public static final String CALLER_VERSION = "o-Call v1.0.1";

    public static final String AUDIO_DATABASE_PATH = "resources/audio.json";

    public static boolean IS_ANY_INSTANCE = numberOfInstance("oCall") > 0;

    private static final int numberOfInstance(String name) {

        InputStream is;
        int numberOfInstance = 0;

        Runtime runtime = Runtime.getRuntime();
        Process process;
        try {
            process = runtime.exec(new String[]{"powershell", "\"gps", "|", "where", "{$_.MainWindowHandle", "-ne", "0", "}", "|", "select Name"});
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        is = process.getInputStream();

        Scanner sc = new Scanner(is);
        try {
            while (sc.hasNext()) {
                String next = sc.next();
                if (name.equals(next)) {
                    numberOfInstance++;
                }
            }
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return numberOfInstance;
    }

}
