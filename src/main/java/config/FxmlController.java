package config;

public abstract class FxmlController {

    public void reset() {
    }

    public void show(boolean show) {
    }

    public void init(Object... object) {
    }
}

