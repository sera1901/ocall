package config;

public class Toggles {

    public static final boolean O_POS_TESTING = getBooleanEnvVariable("CALLER_TESTING", false);


    static boolean getBooleanEnvVariable(String name, boolean defaultValue){

        try {
            if (System.getenv(name) != null) {
                return (System.getenv(name).equals("true")) || defaultValue;
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return defaultValue;
    }
}
