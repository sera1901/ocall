package config;

import com.google.inject.Injector;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.util.Pair;
import view.FxmlView;
import view.PairControllerNode;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public final class GuiceFXMLLoader {

    private final Injector injector;

    @Inject
    public GuiceFXMLLoader(final Injector injector) {
        super();
        this.injector = injector;
    }

    public PairControllerNode load(final FxmlView fxmlView) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getViewLocation(fxmlView));
        loader.setControllerFactory(param -> injector.getInstance(param));

        URL url = getViewLocation(fxmlView);

        InputStream inputStream = url.openStream();

        Parent parent = loader.load(inputStream);

        return new PairControllerNode(parent, loader.getController());
    }


    public PairControllerNode load(FxmlView fxmlView, ObservableList<Node> children) throws IOException {

        final FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getViewLocation(fxmlView));
        loader.setControllerFactory(param -> injector.getInstance(param));

        children.add(loader.load());

        return new PairControllerNode(children.get(children.size()-1), loader.getController());
    }

    private URL getViewLocation(final FxmlView view) {

        try {
            File file = new File("resources" + view.getFxmlFile());

            return file.toURI().toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
