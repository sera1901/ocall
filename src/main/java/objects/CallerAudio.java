package objects;

import com.google.gson.annotations.Expose;

public class CallerAudio {
    @Expose
    private String name;
    @Expose
    private String audioName;
    @Expose
    private CallTime time;
    @Expose
    private Types type;
    @Expose
    private String audioPath;
    @Expose
    private String textRecognition;

    public CallerAudio(String name, String audioName, String timeHour, String timeMinutes,Types type, String text) {
        this.name = name;
        this.audioName = audioName;
        this.time = new CallTime(Integer.valueOf(timeHour), Integer.valueOf(timeMinutes));

        this.type = type;

        if(Types.AUDIO_FILE.equals(type)){
            this.audioPath = text;
        }else{
            this.audioName = "Text/Recognition";
            this.textRecognition = text;
        }
    }

    public void update(String name, String audioName, String timeHour, String timeMinutes, Types type, String text) {
        this.name = name;
        this.audioName = audioName;
        this.time.setHours(Integer.valueOf(timeHour));
        this.time.setMinutes(Integer.valueOf(timeMinutes));


        this.type = type;

        if(Types.AUDIO_FILE.equals(type)){
            this.audioPath = text;
        }else{
            this.audioName = "Text/Recognition";
            this.textRecognition = text;
        }
    }

    public String getName() {
        return name;
    }

    public String getAudioName() {
        return audioName;
    }

    public CallTime getTime() {
        return time;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public Types getType() {
        if(type == null){
            type = Types.AUDIO_FILE;
        }
        return type;
    }

    public String getTextRecognition() {
        return textRecognition;
    }

    public static class CallTime{

        @Expose
        private Integer hours;
        @Expose
        private Integer minutes;

        public CallTime(Integer hour, Integer minutes) {
            this.hours = hour;
            this.minutes = minutes;
        }

        public Integer getHour() {
            return hours;
        }

        public Integer getMinutes() {
            return minutes;
        }

        public void setHours(Integer hours) {
            this.hours = hours;
        }

        public void setMinutes(Integer minutes) {
            this.minutes = minutes;
        }
    }

    public enum Types{
        TEXT_RECONGNITION,
        AUDIO_FILE;

        static Types get(String type){
            return (TEXT_RECONGNITION.name().equals(type)) ? TEXT_RECONGNITION : AUDIO_FILE;
        }
    }
}
