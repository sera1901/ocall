package objects;

public class StagePosition {
    private Double xOffset;
    private Double yOffset;

    public StagePosition(){
        xOffset = 0.0;
        yOffset = 0.0;
    }

    public Double getxOffset() {
        return xOffset;
    }

    public void setxOffset(Double xOffset) {
        this.xOffset = xOffset;
    }

    public Double getyOffset() {
        return yOffset;
    }

    public void setyOffset(Double yOffset) {
        this.yOffset = yOffset;
    }
}