package view;

import com.google.inject.Inject;
import config.FxmlController;
import config.GuiceFXMLLoader;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import objects.StagePosition;

import java.io.IOException;
import java.util.HashMap;

public class StageFxmlViewStagePool implements FxmlViewStagePool {

    private static final HashMap<FxmlView, Stage> stages = new HashMap<>();
    private static final HashMap<FxmlView, Parent> parents = new HashMap<>();
    private static final HashMap<FxmlView, FxmlController> controllers = new HashMap<>();
    private static final HashMap<Integer, FxmlView> controllerHash = new HashMap<>();
    private GuiceFXMLLoader fxmlLoader;


    @Inject
    public StageFxmlViewStagePool(GuiceFXMLLoader fxmlLoader) {
        this.fxmlLoader = fxmlLoader;
    }

    @Override
    public void show(FxmlView fxmlView, Object... objects) {

        Stage stage;

        if (!controllers.containsKey(fxmlView)) {

            stage = new Stage();
            setStage(stage, fxmlView);

        } else {

            stage = getStage(fxmlView);

            if (stage.isShowing()) {
                stage.hide();
            }

            stage.centerOnScreen();
        }

        ((FxmlController) getController(fxmlView)).show(true);

        if (objects.length > 0) {
            ((FxmlController) getController(fxmlView)).init(objects);
        }

        stage.show();

    }


    @Override
    public void showAndWait(FxmlView fxmlView, FxmlView ownerFxml, Object... objects) {

        Stage stage;

        if (!controllers.containsKey(fxmlView)) {

            stage = new Stage();
            setStage(stage, fxmlView);

            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(getStage(ownerFxml));
        } else {

            stage = getStage(fxmlView);

            if (stage.isShowing()) {
                stage.hide();
            }

            stage.centerOnScreen();
        }

        ((FxmlController) getController(fxmlView)).show(true);

        //getParent(ownerFxml).setBlendMode(BlendMode.DIFFERENCE);
        getStage(ownerFxml).setOpacity(0.90D);
        ((FxmlController) getController(fxmlView)).init(objects);
        stage.showAndWait();
        getStage(ownerFxml).setOpacity(1D);
        //getParent(ownerFxml).setBlendMode(BlendMode.SRC_OVER);
    }


    @Override
    public ReadOnlyBooleanProperty getFocusProperty(FxmlView fxmlView) {

        if (controllers.containsKey(fxmlView)) {

            return getStage(fxmlView).focusedProperty();
        }

        return null;
    }


    @Override
    public void close(FxmlView fxmlView) {

        if (controllers.containsKey(fxmlView)) {

            getStage(fxmlView).close();
        }
    }

    @Override
    public void reset(FxmlView fxmlView) {

        if (controllers.containsKey(fxmlView)) {

            ((FxmlController) getController(fxmlView)).reset();
        }
    }

    @Override
    public void init(FxmlView fxmlView, Object... object) {

        if (controllers.containsKey(fxmlView)) {

            ((FxmlController) getController(fxmlView)).init(object);
        }
    }

    @Override
    public Object getController(FxmlView fxmlView) {
        return controllers.get(fxmlView);
    }

    public Stage getStage(FxmlView fxmlView) {
        return stages.get(fxmlView);
    }

    public Parent getParent(FxmlView fxmlView) {
        return parents.get(fxmlView);
    }

    public Stage getStage(Integer hashCode) {
        return stages.get(controllerHash.get(hashCode));
    }

    private boolean setStage(Stage stage, FxmlView fxmlView) {

        if (!controllers.containsKey(fxmlView)) {

            try {

                PairControllerNode pair = fxmlLoader.load(fxmlView);

                Parent root = (Parent) pair.getNode();

                parents.put(fxmlView, root);

                Scene scene = new Scene(root);
                StagePosition stagePosition = new StagePosition();

                stage.setScene(scene);
                stage.setTitle("o-pos " + fxmlView.name());
                stage.initStyle(StageStyle.UNDECORATED);

                root.setOnMousePressed((MouseEvent event) -> {
                    stagePosition.setxOffset(event.getSceneX());
                    stagePosition.setyOffset(event.getSceneY());
                });

                root.setOnMouseDragged((MouseEvent event) -> {
                    stage.setX(event.getScreenX() - stagePosition.getxOffset());
                    stage.setY(event.getScreenY() - stagePosition.getyOffset());
                });

                stages.put(fxmlView, stage);

                controllers.put(fxmlView, pair.getController());

                controllerHash.put(pair.getController().hashCode(), fxmlView);

                return true;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}