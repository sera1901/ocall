package view;

import javafx.beans.property.ReadOnlyBooleanProperty;

public interface FxmlViewStagePool {

    void show(FxmlView fxmlView, Object... objects);

    void close(FxmlView fxmlView);

    void reset(FxmlView fxmlView);

    void init(FxmlView fxmlView, Object...object);

    Object getController(FxmlView fxmlView);

    void showAndWait(FxmlView fxmlView, FxmlView ownerFxml, Object... objects);

    ReadOnlyBooleanProperty getFocusProperty(FxmlView fxmlView);
}
