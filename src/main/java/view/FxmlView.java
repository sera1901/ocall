package view;

public enum FxmlView {

    MAIN{
        @Override
        public String getFxmlFile() {
            return "/fxml/Main.fxml";
        }
    },ADD_EDIT_CALL{
        @Override
        public String getFxmlFile() {
            return "/fxml/AddEditCaller.fxml";
        }
    };

    public abstract String getFxmlFile();
}
