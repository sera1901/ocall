package view;

import com.google.inject.Inject;
import config.FxmlController;
import config.GuiceFXMLLoader;
import javafx.collections.ObservableList;
import javafx.scene.Node;

import java.io.IOException;
import java.util.HashMap;

public class NodeFxmlViewPool implements FxmlViewNodePool {

    private static final HashMap<Integer, Node> nodes = new HashMap<>();
    private static final HashMap<Integer, FxmlController> controllers = new HashMap<>();
    private static final HashMap<Integer, Integer> link = new HashMap<>();

    private GuiceFXMLLoader fxmlLoader;

    @Inject
    public NodeFxmlViewPool(GuiceFXMLLoader fxmlLoader) {
        this.fxmlLoader = fxmlLoader;
    }

    @Override
    public void add(FxmlView fxmlChildren, ObservableList<Node> children) {
        try {
            PairControllerNode pair = fxmlLoader.load(fxmlChildren, children);

            nodes.put(pair.getController().hashCode(), pair.getNode());

            controllers.put(pair.getController().hashCode(), pair.getController());

            link.put(pair.getNode().hashCode(), pair.getController().hashCode());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(FxmlController controller) {
        controllers.remove(controller.hashCode());
    }

    @Override
    public void remove(Node node) {
        nodes.remove(link.get(node.hashCode()));
    }

    @Override
    public void init(FxmlController controller, Object... object) {
        controller.init(object);
    }

    @Override
    public void init(Node node, Object... object) {
        getController(node).init(object);
    }

    @Override
    public void show(FxmlController controller, boolean show) {
        nodes.get(controller.hashCode()).setVisible(show);
        controller.show(show);
    }

    @Override
    public void show(Node node, boolean show) {
        nodes.get(link.get(node.hashCode())).setVisible(show);
        getController(node).show(show);
    }

    @Override
    public void reset(FxmlController controller) {
        controller.reset();
    }

    @Override
    public void reset(Node node) {
        getController(node).reset();
    }

    @Override
    public FxmlController getController(Node node) {
        return controllers.get(link.get(node.hashCode()));
    }

    @Override
    public Node getNode(FxmlController controller) {
        return nodes.get(controller.hashCode());
    }

    public static Node lastNode(ObservableList<Node> children){
        return children.get(children.size()-1);
    }

    public static Node addNode(NodeFxmlViewPool nodeFxmlViewPool, FxmlView fxmlView, ObservableList<Node> children, boolean show, Object ... object){
        nodeFxmlViewPool.add(fxmlView, children);
        nodeFxmlViewPool.show(lastNode(children), show);

        if(object.length > 0){
            nodeFxmlViewPool.init(lastNode(children), object);
        }

        return lastNode(children);
    }
}