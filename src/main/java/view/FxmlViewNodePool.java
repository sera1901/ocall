package view;

import config.FxmlController;
import javafx.collections.ObservableList;
import javafx.scene.Node;

public interface FxmlViewNodePool {

    void add(FxmlView fxmlChildren, ObservableList<Node> children);

    void remove(FxmlController controller);

    void remove(Node node);

    void init(FxmlController controller, Object... object);

    void init(Node node, Object... object);

    void show(FxmlController controller, boolean show);

    void show(Node node, boolean show);

    void reset(FxmlController controller);

    void reset(Node node);

    FxmlController getController(Node node);

    Node getNode(FxmlController controller);
}
