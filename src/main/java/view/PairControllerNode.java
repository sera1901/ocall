package view;

import config.FxmlController;
import javafx.scene.Node;
import javafx.util.Pair;

public class PairControllerNode extends Pair {
    /**
     * Creates a new pair
     *
     * @param node   The key for this pair
     * @param controller The value to use for this pair
     */
    public PairControllerNode(Object node, Object controller) {
        super(node, controller);
    }


    public Node getNode() {
        return (Node)super.getKey();
    }

    public FxmlController getController() {
        return (FxmlController) super.getValue();
    }
}
