package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import objects.CallerAudio;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static config.Config.AUDIO_DATABASE_PATH;

public interface CallerControllerCommon {
    ObservableList<CallerAudio> callAudioList = FXCollections.observableArrayList();
    IntegerProperty selectedAudioIndex = new SimpleIntegerProperty(-1);
    StringProperty voice = new SimpleStringProperty(getFrenchLanguage());

    Gson gson = new GsonBuilder()
            .disableHtmlEscaping()
            .excludeFieldsWithoutExposeAnnotation()
            .setPrettyPrinting()
            .create();

    static void updateAudioDB() throws IOException {
        Writer writer = new FileWriter(AUDIO_DATABASE_PATH);
        gson.toJson(callAudioList.toArray(), writer);
        writer.close();
    }

    static CallerAudio[] getAudioDB() throws IOException {
        JsonReader reader = new JsonReader(new FileReader(AUDIO_DATABASE_PATH));
        CallerAudio[] audioCall = gson.fromJson(reader, CallerAudio[].class);
        reader.close();
        return audioCall;
    }

    static void speechSynthesizerWindow(String text) {

        Runtime runtime = Runtime.getRuntime();

        text = text.replaceAll("'", " ");
        try {
            runtime.exec(new String[]{"PowerShell", "-Command",
                    "\"" +
                            "Add-Type –AssemblyName System.Speech;" +
                            "$speak = New-Object System.Speech.Synthesis.SpeechSynthesizer;" +
                            "$speak.SelectVoice('"+ voice.get() +"');" +
                            "$speak.Speak('" + text + "');" +
                     "\""});
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static List<String> getVoicesName() {
        return getListOf("Name");
    }

    static String getFrenchLanguage() {
        List<String> cultures = getListOf("Culture.Name");

        for(int i = 0; i < cultures.size(); i++){
            if(cultures.get(i).contains("fr")){
                String name = getVoicesName().get(i);
                System.out.println("Selected french language: "+ name);
                return name;
            }
        }

        if(cultures.size() > 0){
            return getVoicesName().get(0);
        }
        return "";
    }

    static List<String> getListOf(String valueName) {

        InputStream is;
        List<String> returnList = new ArrayList<>();

        Runtime runtime = Runtime.getRuntime();
        Process process;
        try {
            process = runtime.exec(new String[]{"PowerShell",
                            "-Command", "\"Add-Type –AssemblyName System.Speech;" +
                            " (New-Object System.Speech.Synthesis.SpeechSynthesizer).GetInstalledVoices().VoiceInfo." + valueName + ";\""
                    }
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        is = process.getInputStream();

        Scanner sc = new Scanner(is);
        try {
            while (sc.hasNextLine()) {
                String next = sc.nextLine();
                System.err.println(next);
                returnList.add(next);
            }
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return returnList;
    }
}
