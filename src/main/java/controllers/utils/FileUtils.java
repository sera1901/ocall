package controllers.utils;

import java.io.*;

public class FileUtils {


    /// getFileContent("resources/styles/dialog.css", "UTF8")
    public static String getFileContent(String path, String encoding) {
        File file = new File(path);

        try( BufferedReader br = new BufferedReader( new InputStreamReader(new FileInputStream(file), encoding )))
        {
            StringBuilder sb = new StringBuilder();
            String line;
            while(( line = br.readLine()) != null ) {
                sb.append( line );
                sb.append( '\n' );
            }
            return sb.toString();
        } catch (IOException e){
            e.printStackTrace();
            return "";
        }
    }
}
