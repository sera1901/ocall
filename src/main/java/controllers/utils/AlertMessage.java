package controllers.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class AlertMessage {
    /**
     * @param header
     * @param content
     */
    public static void showErrorMessage(String header, String content) {

        if (!config.Toggles.O_POS_TESTING) {
            CustomAlert alert = new CustomAlert(Alert.AlertType.ERROR);

            alert.setHeaderText(header);
            alert.setContentText(content);

            alert.showAndWait();
        }
    }

    /**
     * @param header
     * @param content
     */
    public static void showWarningMessage(String header, String content) {

        if (!config.Toggles.O_POS_TESTING) {
            CustomAlert alert = new CustomAlert(Alert.AlertType.WARNING);

            alert.setHeaderText(header);
            alert.setContentText(content);

            alert.showAndWait();
        }
    }

    /**
     * @param header
     * @param content
     */
    public static void showInfoMessage(String header, String content) {

        if (!config.Toggles.O_POS_TESTING) {
            CustomAlert alert = new CustomAlert(Alert.AlertType.INFORMATION);

            alert.setHeaderText(header);
            alert.setContentText(content);

            alert.showAndWait();
        }
    }

    /**
     * @param header
     * @param content
     */
    public static ButtonType showConfirmationMessage(String header, String content, ButtonType... buttons) {

        if (!config.Toggles.O_POS_TESTING) {
            CustomAlert alert = new CustomAlert(Alert.AlertType.CONFIRMATION,"", buttons);

            alert.setHeaderText(header);
            alert.setContentText(content);

            alert.showAndWait();

            return alert.getResult();
        }
        return ButtonType.CANCEL;
    }
}
