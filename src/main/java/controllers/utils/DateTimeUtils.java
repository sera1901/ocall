package controllers.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DateTimeUtils {


    /**
     * @param value
     * @param unit
     * @return
     */
    public static long toMillis(String value, String unit) {
        return toMillisSimple(Long.valueOf(value), ChronoUnit.valueOf(unit));
    }

    /**
     * @param value
     * @param unit
     * @return
     */
    public static long toMillisSimple(Long value, ChronoUnit unit) {

        switch (unit) {
            case SECONDS:
                return Duration.of(value, ChronoUnit.SECONDS).toMillis();
            case HOURS:
                return Duration.of(value, ChronoUnit.HOURS).toMillis();
            case WEEKS:
                return Duration.of(value, ChronoUnit.DAYS).toMillis() * 7;
            case MONTHS:
                return Duration.of(value, ChronoUnit.DAYS).toMillis() * 30;
            case YEARS:
                return Duration.of(value, ChronoUnit.DAYS).toMillis() * 365;
            default: // MINUTES
                return Duration.of(value, ChronoUnit.MINUTES).toMillis();

        }
    }

    /**
     * @param value
     * @return
     */
    public static String fromMillisGetMaxUnit(long value) {

        if ((Duration.of(Long.valueOf(value), ChronoUnit.DAYS).toMillis() * 365) % 365 == 0) {
            return ChronoUnit.YEARS.name();
        }

        if ((Duration.of(Long.valueOf(value), ChronoUnit.DAYS).toMillis() * 30) % 30 == 0) {
            return ChronoUnit.MONTHS.name();
        }

        if ((Duration.of(Long.valueOf(value), ChronoUnit.DAYS).toMillis() * 7) % 7 == 0) {
            return ChronoUnit.WEEKS.name();
        }

        if ((Duration.of(Long.valueOf(value), ChronoUnit.MINUTES).toMillis() * 1440) % 1440 == 0) {
            return ChronoUnit.DAYS.name();
        }

        if ((Duration.of(Long.valueOf(value), ChronoUnit.MINUTES).toMillis() * 60) % 60 == 0) {
            return ChronoUnit.HOURS.name();
        }

        return ChronoUnit.MINUTES.name();
    }

    /**
     * @param value
     * @param unit
     * @return
     */
    public static String fromMillis(long value, String unit) {

        if (ChronoUnit.valueOf(unit).equals(ChronoUnit.YEARS)) {
            return Long.toString(Duration.of(value, ChronoUnit.MILLIS).toDays() / 365);
        }

        if (ChronoUnit.valueOf(unit).equals(ChronoUnit.MONTHS)) {
            return Long.toString(Duration.of(value, ChronoUnit.MILLIS).toDays() / 30);
        }

        if (ChronoUnit.valueOf(unit).equals(ChronoUnit.WEEKS)) {
            return Long.toString(Duration.of(value, ChronoUnit.MILLIS).toDays() / 7);
        }

        if (ChronoUnit.valueOf(unit).equals(ChronoUnit.DAYS)) {
            return Long.toString(Duration.of(value, ChronoUnit.MILLIS).toDays());
        }

        if (ChronoUnit.valueOf(unit).equals(ChronoUnit.HOURS)) {
            return Long.toString(Duration.of(value, ChronoUnit.MILLIS).toHours());
        }

        if (ChronoUnit.valueOf(unit).equals(ChronoUnit.MINUTES)) {
            return Long.toString(Duration.of(value, ChronoUnit.MILLIS).toMinutes());
        }

        return Long.toString(Duration.of(value, ChronoUnit.valueOf(unit)).toMillis());
    }

    /**
     * @param amount
     * @param unit
     * @return
     */
    public static String calculateExpiryDate(int amount, ChronoUnit unit) {
        return LocalDate.now().plus(amount, unit).format(DateTimeFormatter.ofPattern("dd-MMMMMMM-yyyy", Locale.CANADA));
    }

    /**
     * @param date
     * @return
     */
    public static String formatDate(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MMMMMMM-yyyy", Locale.CANADA)).toString();
    }

    /**
     * @param date
     * @return
     */
    public static String formatSqlUtcDate(String date) {

        SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date d = null;
        try {
            d = utcFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat pstFormat = new SimpleDateFormat("dd-MMMMMMM-yyyy HH:mm:ss");

        return pstFormat.format(d);
    }

    /**
     * @param date
     * @return
     */
    public static String formatSqlDate(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    /**
     * @param date
     * @return
     */
    public static String formatSqlUtcTime(String date) {

        SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date d = null;
        try {
            d = utcFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat pstFormat = new SimpleDateFormat("HH:mm:ss");

        return pstFormat.format(d);
    }


    /**
     * @param date
     * @return
     */
    public static String formatSqlDateTime(String date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd-MMMMMMM-yyyy HH:mm:ss", Locale.CANADA);

        Date d = null;

        try {
            d = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedTime = output.format(d);

        return formattedTime;
    }

    /**
     * @return
     */
    public static LocalDate dateNow() {
        return LocalDate.parse(LocalDate.now().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    /**
     * @return
     */
    public static String dateTimeNow() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CANADA);
        return dateFormat.format(new Date());
    }

    /**
     * @return
     */
    public static String dateTimeNowReport() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.CANADA);
        return dateFormat.format(new Date());
    }

    /**
     * @return
     */
    public static String dateDateReport(String date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.CANADA);

        Date d;

        try {
            d = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "2000-01-01T00:00:00";
        }

        return sdf.format(d);
    }

    /**
     * @return
     */
    public static String dateTimeNowSimple() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMMMMMM yyyy HH:mm");
        return dateFormat.format(new Date());
    }

    /**
     * @return
     */
    public static String timeNowSimple() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(new Date());
    }

    /**
     * @return
     */
    public static int getHour() {
        DateFormat dateFormat = new SimpleDateFormat("HH");
        return Integer.parseInt(dateFormat.format(new Date()));
    }

    /**
     * @return
     */
    public static int getMinutes() {
        DateFormat dateFormat = new SimpleDateFormat("mm");
        return Integer.parseInt(dateFormat.format(new Date()));
    }

    /**
     * @return
     */
    public static int getSeconds() {
        DateFormat dateFormat = new SimpleDateFormat("ss");
        return Integer.parseInt(dateFormat.format(new Date()));
    }


    /**
     * @return
     */
    public static LocalDate localDateSqlFormat(String sqlDate) {
        if(sqlDate != null && sqlDate.length() > 9) {
            return LocalDate.parse(sqlDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }else{
            return LocalDate.parse("2000-01-01", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
    }

    /**
     * @return
     */
    public static String fromEditorToSqlFormat(String editor) {
        return LocalDate.parse(
                LocalDate.parse(editor, DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd")
        ).toString();
    }

    /**
     * @return
     */
    public static String sqlDateNow() {
        return LocalDate.parse(LocalDate.now().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString();
    }

    /**
     * @return
     */
    public static String sqlDateNowMinusNDays(long days) {
        LocalDate now = LocalDate.now();
        return LocalDate.parse(now.minusDays(days).toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString();
    }

    /**
     * @return
     */
    public static String dateTimestamp(Long timestamp) {
        return new SimpleDateFormat("dd-MMMMMMM-yyyy", Locale.CANADA).format(new Date(timestamp));
    }

    /**
     * @param string
     * @return
     */
    public static boolean validateDate(String string) {
        return string.matches("^([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))$");
    }

    /**
     * @return
     */
    public static long dayLeft(Long timestamp) {

        LocalDate now = LocalDate.now();
        LocalDate expiry = LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), TimeZone
                .getDefault().toZoneId()).toLocalDate();

        return (now.compareTo(expiry) > 0) ? 0L : ChronoUnit.DAYS.between(now, expiry);
    }

    /**
     * @return
     */
    public static boolean dateOk(Long timestamp) {
        return LocalDate.now().compareTo(LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), TimeZone
                .getDefault().toZoneId()).toLocalDate()) <= 0;
    }

    /**
     * @return
     */
    public static boolean dateOk(String date) {
        return LocalDate.now().compareTo(LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"))) <= 0;
    }

    /**
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<LocalDate> getDatesBetween(
            LocalDate startDate, LocalDate endDate) {

        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(i -> startDate.plusDays(i))
                .collect(Collectors.toList());
    }
}
