package controllers.utils;

import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import objects.StagePosition;

import java.io.File;
import java.net.MalformedURLException;

public class CustomAlert extends Alert {

    private StagePosition stagePosition = new StagePosition();

    /**
     * Creates a new alert with custom settings.
     *
     * @param alertType The underlying alert type.
     * @see Alert
     * @see javafx.scene.control.Alert.AlertType
     */
    public CustomAlert(AlertType alertType) {
        super(alertType);

        setCustomAlert();
    }

    public CustomAlert(AlertType alertType, String contentText, ButtonType... buttons) {
        super(alertType, contentText, buttons);

        setCustomAlert();
    }

    private void setCustomAlert() {

        try {
            super.getDialogPane().getStylesheets().add(
                    new File("resources/styles/dialog.css").toURI().toURL().toExternalForm()
            );
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Button okButton = (Button) super.getDialogPane().lookupButton(ButtonType.OK);

        if(okButton != null){
            okButton.setDefaultButton(false);
        }

        Stage stage = (Stage) super.getDialogPane().getScene().getWindow();
        //stage.getIcons().add(ImageUtils.getImage("oPOS.png"));


        Parent root = super.getDialogPane();

        root.setOnMousePressed((MouseEvent event) -> {
            stagePosition.setxOffset(event.getSceneX());
            stagePosition.setyOffset(event.getSceneY());
        });

        root.setOnMouseDragged((MouseEvent event) -> {
            stage.setX(event.getScreenX() - stagePosition.getxOffset());
            stage.setY(event.getScreenY() - stagePosition.getyOffset());
        });

        super.initStyle(StageStyle.UNDECORATED);
    }
}