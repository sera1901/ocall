package controllers;

import com.google.inject.Inject;
import config.FxmlController;
import controllers.utils.AlertMessage;
import controllers.utils.DateTimeUtils;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;
import objects.CallerAudio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view.FxmlView;
import view.StageFxmlViewStagePool;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static config.Config.CALLER_VERSION;
import static controllers.CallerControllerCommon.callAudioList;

public class CallerMainController extends FxmlController {

    private static Logger logger = LogManager.getLogger(CallerMainController.class);
    private final List<MediaPlayer> players = new ArrayList<>();
    private int oldPlayersIndex = 0;
    @Inject
    private StageFxmlViewStagePool stageFxmlViewPool;
    @FXML
    private TableView<CallerAudio> audioTable;
    @FXML
    private TableColumn<CallerAudio, String> nameColumn, audioColumn, timeColumn;
    @FXML
    private Label timeLabel, version_lbl;

    @FXML
    void onMinimizeAction(ActionEvent event) {
        Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        primaryStage.setIconified(true);
    }

    @FXML
    void onMaximizeAction(ActionEvent event) {
        Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        primaryStage.setMaximized(!primaryStage.isMaximized());
    }

    @FXML
    void onExitAction(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    @FXML
    void onDeleteAction(ActionEvent event) {
        if (!audioTable.getSelectionModel().isEmpty()) {
            callAudioList.remove(audioTable.getSelectionModel().getSelectedIndex());
            try {
                CallerControllerCommon.updateAudioDB();
            } catch (IOException e) {
                logger.error(e);
            }
        } else {
            AlertMessage.showWarningMessage(
                    "Remove",
                    "Please select audio call before clicking remove"
            );
        }
    }

    @FXML
    void onEditAction(ActionEvent event) {
        if (!audioTable.getSelectionModel().isEmpty()) {
            CallerControllerCommon.selectedAudioIndex.set(audioTable.getSelectionModel().getSelectedIndex());
            stageFxmlViewPool.showAndWait(FxmlView.ADD_EDIT_CALL, FxmlView.MAIN, true);

            updateTable();
        } else {
            AlertMessage.showWarningMessage(
                    "Edit",
                    "Please select audio call before clicking edit"
            );
        }
    }

    @FXML
    void onAddAction(ActionEvent event) {
        stageFxmlViewPool.showAndWait(FxmlView.ADD_EDIT_CALL, FxmlView.MAIN, false);
    }

    @FXML
    void onPlayAction(ActionEvent event) {

        if (!audioTable.getSelectionModel().isEmpty()) {

            if(oldPlayersIndex >= 0) {
                players.get(oldPlayersIndex).stop();
            }

            CallerAudio call = callAudioList.get(audioTable.getSelectionModel().getSelectedIndex());
            if(oldPlayersIndex != audioTable.getSelectionModel().getSelectedIndex()){
                if(CallerAudio.Types.TEXT_RECONGNITION.equals(call.getType())){
                    CallerControllerCommon.speechSynthesizerWindow(call.getTextRecognition());
                }else{
                    players.get(audioTable.getSelectionModel().getSelectedIndex()).play();
                }
            }else{
                if(players.get(oldPlayersIndex).isAutoPlay()){
                    if(!CallerAudio.Types.TEXT_RECONGNITION.equals(call.getType())) {
                        players.get(oldPlayersIndex).stop();
                    }
                }else{
                    if(CallerAudio.Types.TEXT_RECONGNITION.equals(call.getType())){
                        CallerControllerCommon.speechSynthesizerWindow(call.getTextRecognition());
                    }else{
                        players.get(audioTable.getSelectionModel().getSelectedIndex()).play();
                    }
                }
            }

            oldPlayersIndex = audioTable.getSelectionModel().getSelectedIndex();

        } else {
            AlertMessage.showWarningMessage(
                    "Play",
                    "Please select audio call before clicking play button"
            );
        }

    }

    @FXML
    void onCallTableClicked(MouseEvent event) {

        if (event.getClickCount() > 1 && !audioTable.getSelectionModel().isEmpty()) {
            CallerControllerCommon.selectedAudioIndex.set(audioTable.getSelectionModel().getSelectedIndex());
            stageFxmlViewPool.showAndWait(FxmlView.ADD_EDIT_CALL, FxmlView.MAIN, true);

            updateTable();
        }
    }

    @FXML
    void onInfoAction(ActionEvent event) {
        CallerControllerCommon.speechSynthesizerWindow("Hello!, I'm a call scheduler. My name is o-Call.");
    }

    private void updateTable() {
        try {
            oldPlayersIndex = -1;
            callAudioList.clear();
            players.clear();
            Collections.addAll(callAudioList, CallerControllerCommon.getAudioDB());

            callAudioList.forEach(callerAudio -> {
                if(CallerAudio.Types.TEXT_RECONGNITION.equals(callerAudio.getType())) {
                    players.add(createPlayer("resources/audio/empty.wav"));
                }else{
                    players.add(createPlayer(callerAudio.getAudioPath()));
                }
            });

        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }


    private MediaPlayer createPlayer(String path) {
        Media media = new Media(new File(path).toURI().toString());
        return new MediaPlayer(media);
    }

    @FXML
    void initialize() {


        version_lbl.setText(CALLER_VERSION);

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        audioColumn.setCellValueFactory(new PropertyValueFactory<>("audioName"));

        timeColumn.setCellValueFactory((TableColumn.CellDataFeatures<CallerAudio, String> p) ->
                new SimpleStringProperty(
                        String.format("%02d:%02d", p.getValue().getTime().getHour(), p.getValue().getTime().getMinutes())
                )
        );

        audioTable.setItems(callAudioList);

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            timeLabel.setText(DateTimeUtils.timeNowSimple());


        }),
                new KeyFrame(Duration.seconds(1))
        );
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();


        Timeline clockMinutes = new Timeline(new KeyFrame(Duration.ZERO, e -> {

            int minutes = DateTimeUtils.getMinutes();
            int hours = DateTimeUtils.getHour();

            for(int i = 0; i < callAudioList.size(); i++){

                if(callAudioList.get(i).getTime().getHour() == hours &&
                   callAudioList.get(i).getTime().getMinutes() == minutes){

                    if(oldPlayersIndex >= 0) {
                        if(!CallerAudio.Types.TEXT_RECONGNITION.equals(callAudioList.get(oldPlayersIndex).getType())) {
                            players.get(oldPlayersIndex).stop();
                        }
                    }

                    oldPlayersIndex = i;

                    if(CallerAudio.Types.TEXT_RECONGNITION.equals(callAudioList.get(i).getType())) {
                        CallerControllerCommon.speechSynthesizerWindow(callAudioList.get(i).getTextRecognition());
                    }else{
                        players.get(i).play();
                    }

                    break;
                }
            }


        }),
                new KeyFrame(Duration.seconds(59))
        );
        clockMinutes.setCycleCount(Animation.INDEFINITE);
        clockMinutes.play();

    }

    @Override
    public void show(boolean show) {

        updateTable();
    }
}
