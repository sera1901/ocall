package controllers;

import config.FxmlController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import objects.CallerAudio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

import static controllers.CallerControllerCommon.callAudioList;

public class AddEditCallerController extends FxmlController {

    private static Logger logger = LogManager.getLogger(AddEditCallerController.class);

    @FXML
    private TextField nameField;

    @FXML
    private ChoiceBox<String> hoursChoiceBox, minutesChoiceBox;

    @FXML
    private Label audioPathLabel;

    @FXML
    private RadioButton audioFileRadio, textRecognitionRadio;

    @FXML
    private HBox audioFileHBox;

    @FXML
    private VBox recognitionVBox;

    @FXML
    private TextArea recognitionTextArea;

    private FileChooser fileChooser = new FileChooser();

    private boolean addNew;

    private String audioFileName;


    private boolean inputOk() {
        return nameField.getText().length() > 0 &&
                ((audioPathLabel.getText().length() > 1 && audioFileRadio.isSelected()) ||
                (recognitionTextArea.getText().trim().length() > 0 && textRecognitionRadio.isSelected()));
    }

    @FXML
    void onCloseWindowAction(ActionEvent event) {
        ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
    }

    @FXML
    void onFileChooseAction(ActionEvent event) {
        File selectedFile = fileChooser.showOpenDialog(((Node) event.getSource()).getScene().getWindow());

        if (selectedFile != null) {

            String relativePath = new File(System.getProperty("user.dir")).toURI().relativize(selectedFile.toURI()).getPath();

            audioFileName = selectedFile.getName();

            audioPathLabel.setText(relativePath);

        }
    }

    @FXML
    void onTestRecognitionAction(ActionEvent event) {

        if(recognitionTextArea.getText().trim().length() > 0){
            CallerControllerCommon.speechSynthesizerWindow(recognitionTextArea.getText());
        }
    }

    @FXML
    void onTextRecognitionAction(ActionEvent event) {

        audioFileRadio.setSelected(!textRecognitionRadio.isSelected());
        audioFileHBox.setVisible(!textRecognitionRadio.isSelected());
        recognitionVBox.setVisible(textRecognitionRadio.isSelected());
    }

    @FXML
    void onAudioFileAction(ActionEvent event) {

        textRecognitionRadio.setSelected(!audioFileRadio.isSelected());
        audioFileHBox.setVisible(!textRecognitionRadio.isSelected());
        recognitionVBox.setVisible(textRecognitionRadio.isSelected());
    }


    @FXML
    void onCancelAction(ActionEvent event) {
        ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
    }

    @FXML
    void onSaveAction(ActionEvent event) {

        if (addNew) {

            if (inputOk()) {
                callAudioList.add(
                        new CallerAudio(
                                nameField.getText(),
                                audioFileName,
                                hoursChoiceBox.getValue(),
                                minutesChoiceBox.getValue(),
                                (audioFileRadio.isSelected()) ? CallerAudio.Types.AUDIO_FILE : CallerAudio.Types.TEXT_RECONGNITION,
                                (audioFileRadio.isSelected()) ? audioPathLabel.getText() : recognitionTextArea.getText()
                        )
                );

                try {
                    CallerControllerCommon.updateAudioDB();
                } catch (IOException e) {
                    logger.error(e);
                }

                ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
            }
        } else {
            if (inputOk()) {
                callAudioList.get(CallerControllerCommon.selectedAudioIndex.get()).update(
                        nameField.getText(),
                        audioFileName,
                        hoursChoiceBox.getValue(),
                        minutesChoiceBox.getValue(),
                        (audioFileRadio.isSelected()) ? CallerAudio.Types.AUDIO_FILE : CallerAudio.Types.TEXT_RECONGNITION,
                        (audioFileRadio.isSelected()) ? audioPathLabel.getText() : recognitionTextArea.getText()
                );

                try {
                    CallerControllerCommon.updateAudioDB();
                } catch (IOException e) {
                    logger.error(e);
                }

                ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
            }
        }
    }

    @FXML
    void initialize() {

        for (int i = 0; i < 24; i++) {
            hoursChoiceBox.getItems().add(String.valueOf(i));
        }

        for (int i = 0; i < 60; i++) {
            minutesChoiceBox.getItems().add(String.valueOf(i));
        }

        hoursChoiceBox.getSelectionModel().select("0");
        minutesChoiceBox.getSelectionModel().select("0");

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("WAV File", "*.wav")
        );

    }

    @Override
    public void init(Object... object) {

        addNew = true;

        if (object.length > 0 && (boolean) object[0]) {

            CallerAudio call = callAudioList.get(CallerControllerCommon.selectedAudioIndex.get());

            nameField.setText(call.getName());
            hoursChoiceBox.setValue(call.getTime().getHour().toString());
            minutesChoiceBox.setValue(call.getTime().getMinutes().toString());
            addNew = false;

            audioFileRadio.setSelected(CallerAudio.Types.AUDIO_FILE.equals(call.getType()));

            if(CallerAudio.Types.AUDIO_FILE.equals(call.getType())){
                audioPathLabel.setText(call.getAudioPath());
            }else{
                audioPathLabel.setText("");
                recognitionTextArea.setText(call.getTextRecognition());
            }


        } else {
            nameField.setText("");
            hoursChoiceBox.setValue("0");
            minutesChoiceBox.setValue("0");
            audioPathLabel.setText("");
            recognitionTextArea.setText("");
            audioFileRadio.setSelected(true);
        }



        textRecognitionRadio.setSelected(!audioFileRadio.isSelected());
        audioFileHBox.setVisible(audioFileRadio.isSelected());
        recognitionVBox.setVisible(!audioFileRadio.isSelected());
    }
}
